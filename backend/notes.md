## Prisma

- To deploy the changes after changing the schema:
`npm run deploy`

## Adding a piece of data.
- Add a type to our datamodel.graphql (The prisma schema that provides us filtering, pagination and stuff).
- Deploy to Prisma and our generated prisma.graphql is going to be updated with paginatinon, etc.
- Write the mutations/queries that are going to be queried from our clients in schema.graphql.
- Write the resolver query/mutation to add any custom logic as well as persist to our data storage.


# Schema
createItem mutation props are not required, should them? 
