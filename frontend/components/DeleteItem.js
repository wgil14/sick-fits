import React, { Component } from 'react';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import { ALL_ITEMS_QUERY } from './Items';

const DELETE_ITEM_MUTATION = gql`
  mutation DELETE_ITEM_MUTATION($id: ID!) {
    deleteItem(id: $id) {
      id
    }
  }
`;

class DeleteButton extends Component {
  update = (cache, payload) => {
    // Manually update the cache on the client.
    // 1.- Read the cache for the items we want.
    const data = cache.readQuery({ query: ALL_ITEMS_QUERY });
    // 2.- Filter the deleted items out of the cache.
    data.items = data.items.filter(item => item.id !== payload.data.deleteItem.id);
    // 3.- Put the items back to the page.
    cache.writeQuery({ query: ALL_ITEMS_QUERY, data });
  };

  render() {
    const { children, id } = this.props;
    return (
      <Mutation mutation={DELETE_ITEM_MUTATION} variables={{ id }} update={this.update}>
        {(deleteItem, { error }) => (
          <button
            type="button"
            onClick={() => {
              if (confirm('Are you sure you want to delete this item?')) {
                deleteItem().catch(e => alert(e.message));
              }
            }}
          >
            {children}
          </button>
        )}
      </Mutation>
    );
  }
}

export default DeleteButton;
