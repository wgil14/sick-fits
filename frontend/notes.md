## Styled Components

Use `ThemeProvider` to provide global variables like colors, etc.

```js
const theme = {
  red: '#FF0000',
  black: '#393939',
  grey: '#3A3A3A',
  lightgrey: '#E1E1E1',
  offWhite: '#EDEDED',
  maxWidth: '1000px',
  bs: '0 12px 24px 0 rgba(0, 0, 0, 0.09)'
};
```

Use `injectGlobal` to provide global styles.

```js
injectGlobal`
  html {
    box-sizing: border-box;
    font-size: 10px; // We can calculate rem unit base 10.
  }
  *, *:before, *:after {
    box-sizing: inherit;
  }
`;
```

## Loading bar

- There's a NProgress bar that starts/end on router events.

## Misc

- Images are uploaded to a service called Cloudinary. We can compress and scale images
  there.

- Does anybody know if `Components/styles/NavStyles.js#38` hardcoded color value is intentionally or it should be read from the theme?

- React file inputs are uncontrolled https://reactjs.org/docs/uncontrolled-components.html.

- We're uploading files onChange, If the user doesn't send the form, the image would be orphan.

- In our `updateItem` mutation we set that the response has a required Item but not using it at all. Is it worth having that response as a required?

- Next.js errors are hard to debug, it would be great to have some hints...

##GraphQL

- Added a .graphqlconfig file to execute queries from the editor.

## Apollo

Adding or deleting items doesn't update the cache and affects pagination.
Possible solutions:

- Ignore the cache for the query using `fetchPolicy="network-only" Don't use the cache`.
  Currently there's no way to invalidate part of the cache or to handle this situation in a better way. This is going to be added to Apollo in the future.
